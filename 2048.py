import os
from random import randint

size = 4
matrix = [[0 for x in range(size)] for y in range(size)]
score = 0


def printMatrix():
    print(f"Score: {score}\n")
    for i in range(size):
        for j in range(size):
            print(str(matrix[i][j]).ljust(3), " ", end="")
        print("\n", end="")


def addRandomToMatrix():
    freeIndexes = []

    for i in range(size):
        for j in range(size):
            if matrix[i][j] == 0:
                freeIndexes.append([i, j])

    if len(freeIndexes):
        index = freeIndexes[randint(0, len(freeIndexes) - 1)]
        matrix[index[0]][index[1]] = randint(1, 2) * 2


def addUp():
    global score
    for xIdx in range(size):
        for yIdx in range(size):
            if matrix[yIdx][xIdx] != 0:
                for yIdx2 in range(yIdx + 1, size):
                    if matrix[yIdx2][xIdx] == 0:
                        continue
                    elif matrix[yIdx2][xIdx] == matrix[yIdx][xIdx]:
                        matrix[yIdx][xIdx] *= 2
                        matrix[yIdx2][xIdx] = 0
                        score += matrix[yIdx][xIdx]
                        break
                    else:
                        break


def shiftUp():
    for xIdx in range(size):
        for yIdx in range(size):
            if matrix[yIdx][xIdx] == 0:
                for yIdx2 in range(yIdx + 1, size):
                    if matrix[yIdx2][xIdx] != 0:
                        matrix[yIdx][xIdx] = matrix[yIdx2][xIdx]
                        matrix[yIdx2][xIdx] = 0
                        break


def addDown():
    global score
    for xIdx in range(size):
        for yIdx in range(size - 1, -1, -1):
            if matrix[yIdx][xIdx] != 0:
                for yIdx2 in range(yIdx - 1, -1, -1):
                    if matrix[yIdx2][xIdx] == 0:
                        continue
                    elif matrix[yIdx2][xIdx] == matrix[yIdx][xIdx]:
                        matrix[yIdx][xIdx] *= 2
                        matrix[yIdx2][xIdx] = 0
                        score += matrix[yIdx][xIdx]
                        break
                    else:
                        break


def shiftDown():
    for xIdx in range(size):
        for yIdx in range(size - 1, -1, -1):
            if matrix[yIdx][xIdx] == 0:
                for yIdx2 in range(yIdx - 1, -1, -1):
                    if matrix[yIdx2][xIdx] != 0:
                        matrix[yIdx][xIdx] = matrix[yIdx2][xIdx]
                        matrix[yIdx2][xIdx] = 0
                        break


def addRight():
    global score
    for yIdx in range(size):
        for xIdx in range(size - 1, -1, -1):
            if matrix[yIdx][xIdx] != 0:
                for xIdx2 in range(xIdx - 1, -1, -1):
                    if matrix[yIdx][xIdx2] == 0:
                        continue
                    elif matrix[yIdx][xIdx2] == matrix[yIdx][xIdx]:
                        matrix[yIdx][xIdx] *= 2
                        matrix[yIdx][xIdx2] = 0
                        score += matrix[yIdx][xIdx]
                        break
                    else:
                        break


def shiftRight():
    for yIdx in range(size):
        for xIdx in range(size - 1, -1, -1):
            if matrix[yIdx][xIdx] == 0:
                for xIdx2 in range(xIdx - 1, -1, -1):
                    if matrix[yIdx][xIdx2] != 0:
                        matrix[yIdx][xIdx] = matrix[yIdx][xIdx2]
                        matrix[yIdx][xIdx2] = 0
                        break


def addLeft():
    global score
    for yIdx in range(size):
        for xIdx in range(size):
            if matrix[yIdx][xIdx] != 0:
                for xIdx2 in range(xIdx + 1, size):
                    if matrix[yIdx][xIdx2] == 0:
                        continue
                    elif matrix[yIdx][xIdx2] == matrix[yIdx][xIdx]:
                        matrix[yIdx][xIdx] *= 2
                        matrix[yIdx][xIdx2] = 0
                        score += matrix[yIdx][xIdx]
                        break
                    else:
                        break


def shiftLeft():
    for yIdx in range(size):
        for xIdx in range(size):
            if matrix[yIdx][xIdx] == 0:
                for xIdx2 in range(xIdx + 1, size):
                    if matrix[yIdx][xIdx2] != 0:
                        matrix[yIdx][xIdx] = matrix[yIdx][xIdx2]
                        matrix[yIdx][xIdx2] = 0
                        break


def updateMatrix(direction):
    if direction == "w":
        addUp()
        shiftUp()

    elif direction == "s":
        addDown()
        shiftDown()

    elif direction == "a":
        addLeft()
        shiftLeft()

    elif direction == "d":
        addRight()
        shiftRight()

addRandomToMatrix()
addRandomToMatrix()

while True:
    os.system("clear")

    printMatrix()

    key = input("")
    while (key != "w") and (key != "a") and (key != "s") and (key != "d"):
        key = input("")

    updateMatrix(key)

    addRandomToMatrix()
